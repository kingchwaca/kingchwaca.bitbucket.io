Enterprise Code Repository
==========================

Welcome to the Enterprise Code Repository (ECR).  This comprehensive set of documentation represents all code that is suitable for reuse within Leidos.

Governance
----------

Some governance stuff goes here, with links to the Leidos Governance code reuse web site.

.. toctree::
   :maxdepth: 2

   ./governance

Take Part
---------

If you would like to share your code with the rest of Leidos, follow these easy steps.

* Read and follow the Governance Model
* Make sure your code is secure
* Implement documentation in a docs folder

Projects In the Reuse Library
-----------------------------

.. toctree::
   :maxdepth: 2

   ./projects
